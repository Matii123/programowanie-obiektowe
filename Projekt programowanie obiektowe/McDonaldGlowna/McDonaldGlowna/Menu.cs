﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class Menu : UserControl
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            BtBurgery.Font = new Font(BtBurgery.Font, FontStyle.Bold);
        }

        private void PrzyciskBurgery_MouseLeave(object sender, EventArgs e)
        {
            BtBurgery.Font = new Font(BtBurgery.Font, FontStyle.Regular);
        }

        private void PrzyciskBurgery_Click(object sender, EventArgs e) // odwolanie do burgery.cs
        {
            burgery1.BringToFront();
        }

        private void button3_Click(object sender, EventArgs e) // itd..
        {
            popularne1.BringToFront();
        }

        private void Przycisknapoj_Click(object sender, EventArgs e)
        {
            napoj1.BringToFront();
        }

        private void Przyciskdesery_Click(object sender, EventArgs e)
        {
            desery1.BringToFront();
        }
    }
}
