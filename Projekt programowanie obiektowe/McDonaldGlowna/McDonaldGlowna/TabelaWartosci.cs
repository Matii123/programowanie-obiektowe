﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class TabelaWartosci : UserControl
    {
        Kalorieproduktow CocaCola = new Kalorieproduktow("CocaCola", 100, 10, 0, 0, 41);
        Kalorieproduktow Fanta = new Kalorieproduktow("Fanta", 100, 9, 0, 0, 38);
        Kalorieproduktow Sprite = new Kalorieproduktow("Sprite", 100, 8, 0, 0, 32);
        Kalorieproduktow Woda = new Kalorieproduktow("Woda mineralna", 100, 0, 0, 0, 0);
        Kalorieproduktow BigMac = new Kalorieproduktow("BigMac", 100, 15, 5, 11, 231);
        Kalorieproduktow McRoyal = new Kalorieproduktow("McRoyal", 100, 13, 5, 9, 211);
        Kalorieproduktow WieśMac = new Kalorieproduktow("WieśMac", 100, 11, 3, 8, 201);
        Kalorieproduktow McChicken = new Kalorieproduktow("McChicken", 100, 10, 6, 12, 198);
        Kalorieproduktow McDouble = new Kalorieproduktow("McDouble", 100, 15, 12, 15, 240);
        Kalorieproduktow Cheeseburger = new Kalorieproduktow("Cheesenurger", 100, 8, 2, 6, 150);
        Kalorieproduktow Chikker = new Kalorieproduktow("Chikker", 100, 10, 8, 6, 180);
        Kalorieproduktow Kurczakburger = new Kalorieproduktow("Kurczakburger", 100, 5, 5, 7, 160);
        Kalorieproduktow Jalapeñoburger = new Kalorieproduktow("Jalapeño Kalorieproduktow", 100, 6, 3, 11, 132);
        Kalorieproduktow McFlurry = new Kalorieproduktow("McFlurry", 100, 15, 5, 11, 231);
        Kalorieproduktow Lody = new Kalorieproduktow("Lody z polewa", 100, 3, 4, 5, 54);
        Kalorieproduktow CiastkoSezonowe = new Kalorieproduktow("Ciastko Sezonowe", 100, 15, 5, 11, 231);
        Kalorieproduktow Owocojogurt = new Kalorieproduktow("Owocojogurt", 100, 15, 5, 11, 231);
        Kalorieproduktow LodoweMarzenie = new Kalorieproduktow("Lodowe Marzenie", 100, 15, 5, 11, 231);
        Kalorieproduktow Shake = new Kalorieproduktow("Shake", 100, 15, 5, 11, 231);

        LiczenieKalori liczeniekalori = new LiczenieKalori();
        List<Kalorieproduktow> produkty_kalorie = new List<Kalorieproduktow>();

        


        public TabelaWartosci() //Dodawanie produktów do listy
        {
            InitializeComponent();
            produkty_kalorie.Add(CocaCola);
            produkty_kalorie.Add(Fanta);
            produkty_kalorie.Add(Sprite);
            produkty_kalorie.Add(BigMac);
            produkty_kalorie.Add(McRoyal);
            produkty_kalorie.Add(McFlurry);
            produkty_kalorie.Add(WieśMac);
            produkty_kalorie.Add(Lody);
            produkty_kalorie.Add(CiastkoSezonowe);
            produkty_kalorie.Add(Owocojogurt);
            produkty_kalorie.Add(LodoweMarzenie);
            produkty_kalorie.Add(Shake);
            produkty_kalorie.Add(McChicken);
            produkty_kalorie.Add(McDouble);
            produkty_kalorie.Add(Cheeseburger);
            produkty_kalorie.Add(Woda);
            produkty_kalorie.Add(Chikker);
            produkty_kalorie.Add(Kurczakburger);
            produkty_kalorie.Add(Jalapeñoburger);


            //dodadanie produktow do listy rozwijanej
            foreach (var i in produkty_kalorie)
            {
                WarLista1.Items.Add(i.nazwa);

            }


        }



        private void Oblicz_Click(object sender, EventArgs e) //Wypisywanie informacji o produkcie
        {

            var a = WarLista1.SelectedItem;

            foreach (var i in produkty_kalorie)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    TextWar1.Text = i.Informacje();
                }

            }

            foreach (var i in produkty_kalorie)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    TextWar2.Text = i.Informacje1() +"kcal";
                }
                
            }
            foreach (var i in produkty_kalorie)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    TextWar3.Text = i.Informacje2()+"gr";
                }

            }
            foreach (var i in produkty_kalorie)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    TextWar4.Text = i.Informacje3()+"gr";
                }

            }
            foreach (var i in produkty_kalorie)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    TextWar5.Text = i.Informacje4() +"gr";
                }

            }
            foreach (var i in produkty_kalorie)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    TextWar5.Text = i.Informacje4() +"gr";
                }

            }

        }

        
    }
}

