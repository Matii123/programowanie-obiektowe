﻿namespace McDonaldGlowna
{
    partial class Restauracje
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.wyszukaj = new System.Windows.Forms.Button();
            this.Listamiast = new System.Windows.Forms.ComboBox();
            this.napis = new System.Windows.Forms.TextBox();
            this.mapa = new System.Windows.Forms.WebBrowser();
            this.LbRestauracje = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.White;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(147, 460);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // wyszukaj
            // 
            this.wyszukaj.BackColor = System.Drawing.Color.White;
            this.wyszukaj.FlatAppearance.BorderSize = 0;
            this.wyszukaj.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.wyszukaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.wyszukaj.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyszukaj.Location = new System.Drawing.Point(6, 177);
            this.wyszukaj.Name = "wyszukaj";
            this.wyszukaj.Size = new System.Drawing.Size(113, 33);
            this.wyszukaj.TabIndex = 1;
            this.wyszukaj.Text = "wyszukaj";
            this.wyszukaj.UseVisualStyleBackColor = false;
            this.wyszukaj.Click += new System.EventHandler(this.wyszukaj_Click);
            // 
            // Listamiast
            // 
            this.Listamiast.BackColor = System.Drawing.Color.White;
            this.Listamiast.DropDownHeight = 68;
            this.Listamiast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Listamiast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Listamiast.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Listamiast.ForeColor = System.Drawing.Color.Goldenrod;
            this.Listamiast.FormattingEnabled = true;
            this.Listamiast.IntegralHeight = false;
            this.Listamiast.Items.AddRange(new object[] {
            "Warszawa",
            "Kraków",
            "Olsztyn",
            "Gdańsk",
            "Białystok",
            "Opole",
            "Poznań",
            "Gdynia",
            "Zielona Góra",
            "Bydgoszcz",
            "Lublin",
            "Łódź",
            "Wrocław"});
            this.Listamiast.Location = new System.Drawing.Point(3, 145);
            this.Listamiast.Name = "Listamiast";
            this.Listamiast.Size = new System.Drawing.Size(132, 26);
            this.Listamiast.TabIndex = 2;
            // 
            // napis
            // 
            this.napis.Location = new System.Drawing.Point(18, 62);
            this.napis.Name = "napis";
            this.napis.Size = new System.Drawing.Size(114, 20);
            this.napis.TabIndex = 3;
            this.napis.Text = "McDonald";
            // 
            // mapa
            // 
            this.mapa.Location = new System.Drawing.Point(189, 49);
            this.mapa.MinimumSize = new System.Drawing.Size(20, 20);
            this.mapa.Name = "mapa";
            this.mapa.ScriptErrorsSuppressed = true;
            this.mapa.Size = new System.Drawing.Size(749, 408);
            this.mapa.TabIndex = 4;
            // 
            // LbRestauracje
            // 
            this.LbRestauracje.AutoSize = true;
            this.LbRestauracje.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbRestauracje.Location = new System.Drawing.Point(325, 6);
            this.LbRestauracje.Name = "LbRestauracje";
            this.LbRestauracje.Size = new System.Drawing.Size(338, 40);
            this.LbRestauracje.TabIndex = 5;
            this.LbRestauracje.Text = "Nasze Restauracje";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Wybierz Miasto:";
            // 
            // Restauracjee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LbRestauracje);
            this.Controls.Add(this.mapa);
            this.Controls.Add(this.Listamiast);
            this.Controls.Add(this.wyszukaj);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.napis);
            this.Name = "Restauracjee";
            this.Size = new System.Drawing.Size(938, 460);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button wyszukaj;
        private System.Windows.Forms.ComboBox Listamiast;
        private System.Windows.Forms.TextBox napis;
        private System.Windows.Forms.WebBrowser mapa;
        private System.Windows.Forms.Label LbRestauracje;
        private System.Windows.Forms.Label label2;
    }
}
