﻿namespace McDonaldGlowna
{
    partial class Menu
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtNowosci = new System.Windows.Forms.Button();
            this.BtBurgery = new System.Windows.Forms.Button();
            this.BtNapoje = new System.Windows.Forms.Button();
            this.BtDesery = new System.Windows.Forms.Button();
            this.popularne1 = new McDonaldGlowna.Popularne();
            this.napoj1 = new McDonaldGlowna.Napoje();
            this.desery1 = new McDonaldGlowna.Desery();
            this.burgery1 = new McDonaldGlowna.Burgery();
            this.SuspendLayout();
            // 
            // BtNowosci
            // 
            this.BtNowosci.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtNowosci.FlatAppearance.BorderSize = 0;
            this.BtNowosci.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BtNowosci.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtNowosci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtNowosci.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtNowosci.ForeColor = System.Drawing.Color.Maroon;
            this.BtNowosci.Location = new System.Drawing.Point(3, 0);
            this.BtNowosci.Name = "BtNowosci";
            this.BtNowosci.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BtNowosci.Size = new System.Drawing.Size(231, 52);
            this.BtNowosci.TabIndex = 2;
            this.BtNowosci.Text = "Nowość";
            this.BtNowosci.UseVisualStyleBackColor = true;
            this.BtNowosci.Click += new System.EventHandler(this.button3_Click);
            // 
            // BtBurgery
            // 
            this.BtBurgery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtBurgery.FlatAppearance.BorderSize = 0;
            this.BtBurgery.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtBurgery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtBurgery.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtBurgery.ForeColor = System.Drawing.Color.Maroon;
            this.BtBurgery.Location = new System.Drawing.Point(252, 0);
            this.BtBurgery.Name = "BtBurgery";
            this.BtBurgery.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BtBurgery.Size = new System.Drawing.Size(231, 52);
            this.BtBurgery.TabIndex = 9;
            this.BtBurgery.Text = "Burgery";
            this.BtBurgery.UseVisualStyleBackColor = true;
            this.BtBurgery.Click += new System.EventHandler(this.PrzyciskBurgery_Click);
            // 
            // BtNapoje
            // 
            this.BtNapoje.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtNapoje.FlatAppearance.BorderSize = 0;
            this.BtNapoje.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtNapoje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtNapoje.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtNapoje.ForeColor = System.Drawing.Color.DarkRed;
            this.BtNapoje.Location = new System.Drawing.Point(478, 0);
            this.BtNapoje.Name = "BtNapoje";
            this.BtNapoje.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BtNapoje.Size = new System.Drawing.Size(231, 52);
            this.BtNapoje.TabIndex = 10;
            this.BtNapoje.Text = "Napoje";
            this.BtNapoje.UseVisualStyleBackColor = true;
            this.BtNapoje.Click += new System.EventHandler(this.Przycisknapoj_Click);
            // 
            // BtDesery
            // 
            this.BtDesery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtDesery.FlatAppearance.BorderSize = 0;
            this.BtDesery.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtDesery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtDesery.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtDesery.ForeColor = System.Drawing.Color.Maroon;
            this.BtDesery.Location = new System.Drawing.Point(715, 3);
            this.BtDesery.Name = "BtDesery";
            this.BtDesery.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BtDesery.Size = new System.Drawing.Size(231, 52);
            this.BtDesery.TabIndex = 11;
            this.BtDesery.Text = "Desery";
            this.BtDesery.UseVisualStyleBackColor = true;
            this.BtDesery.Click += new System.EventHandler(this.Przyciskdesery_Click);
            // 
            // popularne1
            // 
            this.popularne1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.popularne1.Location = new System.Drawing.Point(44, 58);
            this.popularne1.Name = "popularne1";
            this.popularne1.Size = new System.Drawing.Size(853, 359);
            this.popularne1.TabIndex = 8;
            // 
            // napoj1
            // 
            this.napoj1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.napoj1.Location = new System.Drawing.Point(44, 61);
            this.napoj1.Name = "napoj1";
            this.napoj1.Size = new System.Drawing.Size(853, 356);
            this.napoj1.TabIndex = 13;
            // 
            // desery1
            // 
            this.desery1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.desery1.Location = new System.Drawing.Point(44, 61);
            this.desery1.Name = "desery1";
            this.desery1.Size = new System.Drawing.Size(838, 356);
            this.desery1.TabIndex = 14;
            // 
            // burgery1
            // 
            this.burgery1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.burgery1.Location = new System.Drawing.Point(44, 58);
            this.burgery1.Name = "burgery1";
            this.burgery1.Size = new System.Drawing.Size(838, 356);
            this.burgery1.TabIndex = 15;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.popularne1);
            this.Controls.Add(this.BtDesery);
            this.Controls.Add(this.BtNapoje);
            this.Controls.Add(this.BtBurgery);
            this.Controls.Add(this.BtNowosci);
            this.Controls.Add(this.burgery1);
            this.Controls.Add(this.napoj1);
            this.Controls.Add(this.desery1);
            this.Name = "Menu";
            this.Size = new System.Drawing.Size(953, 434);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BtNowosci;
        private Popularne popularne1;
        private System.Windows.Forms.Button BtBurgery;
        private System.Windows.Forms.Button BtNapoje;
        private System.Windows.Forms.Button BtDesery;
        private Napoje napoj1;
        private Desery desery1;
        private Burgery burgery1;
    }
}
