﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class Stronaglowna : UserControl
    {
        public Stronaglowna()
        {
            InitializeComponent();
        }
        private void strglowna_Load(object sender, EventArgs e)
        {
            czasslider.Start();
        }
        private void czasslider_Tick(object sender, EventArgs e)
        {
            if (obrazslider1.Visible == true)
            {
                obrazslider1.Visible = false;
                obrazslider2.Visible = true;
            }
            else if (obrazslider2.Visible == true)
            {
                obrazslider2.Visible = false;
                obrazslider5.Visible = true;
            }
            else if (obrazslider5.Visible == true)
            {
                obrazslider5.Visible = false;
                obrazslider1.Visible = true;
            }
          
        }
    }
}
