﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonaldGlowna
{
    class Koszyk: IKoszyk
    {
        public List<Produkt> produkty_koszyk;
        public List<int> ilosc_produktow;
        public int numerek_zamowienia = 0;

        public Koszyk()
        {
            this.produkty_koszyk = new List<Produkt>();
            this.ilosc_produktow = new List<int>();
        }
        //Dodanie produktu wraz z iloscia do koszyka
        public void Dodajdokoszyka(Produkt produkt, int ilosc)
        {
            this.produkty_koszyk.Add(produkt);
            this.ilosc_produktow.Add(ilosc);
        }

        //Laczny koszt do zaplaty
        public int Koszt()
        {
            int suma = 0;
            for (int i = 0; i < produkty_koszyk.Count(); i++)
            {
                suma += this.produkty_koszyk[i].cena * ilosc_produktow[i];
            }
            return suma;
        }
        //Wypisanie wszystkich produktow w koszyku
        public string WyswietlProdukty()
        {
            string lista = "";
            int a = 0;
            foreach (Produkt p in this.produkty_koszyk)
            {
                lista += p.WypiszInfo() + " Ilosc: " + ilosc_produktow[a] + "\n";
                a++;
            }
            return lista;
        }
        //Dodanie zamowienia
        public void DodajNumerek()
        {
            this.numerek_zamowienia++;
        }
    }
}
