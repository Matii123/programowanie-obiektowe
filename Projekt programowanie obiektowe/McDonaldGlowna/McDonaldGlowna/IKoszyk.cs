﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonaldGlowna
{
    interface IKoszyk
    {
        void Dodajdokoszyka(Produkt produkt, int ilosc);
        int Koszt();
        string WyswietlProdukty();
        void DodajNumerek();

    }
}
