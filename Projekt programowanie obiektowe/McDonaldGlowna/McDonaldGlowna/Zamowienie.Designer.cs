﻿namespace McDonaldGlowna
{
    partial class Zamowienie
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.CbLista1 = new System.Windows.Forms.ComboBox();
            this.CbLista2 = new System.Windows.Forms.ComboBox();
            this.CbLista3 = new System.Windows.Forms.ComboBox();
            this.CbLista4 = new System.Windows.Forms.ComboBox();
            this.LbZamowienie = new System.Windows.Forms.Label();
            this.TbIlosc1 = new System.Windows.Forms.TextBox();
            this.TbIlosc2 = new System.Windows.Forms.TextBox();
            this.TbIlosc3 = new System.Windows.Forms.TextBox();
            this.TbIlosc4 = new System.Windows.Forms.TextBox();
            this.LbProdukt = new System.Windows.Forms.Label();
            this.LbIlosc = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TbCena = new System.Windows.Forms.TextBox();
            this.BtPrzelicz = new System.Windows.Forms.Button();
            this.LbPlatnosc = new System.Windows.Forms.Label();
            this.CbPlatnosc = new System.Windows.Forms.ComboBox();
            this.CbNawynos = new System.Windows.Forms.ComboBox();
            this.LbNawynos = new System.Windows.Forms.Label();
            this.BtZatwierdz = new System.Windows.Forms.Button();
            this.LbPodsumowanie = new System.Windows.Forms.Label();
            this.LbProdukt2 = new System.Windows.Forms.Label();
            this.LbLista = new System.Windows.Forms.Label();
            this.LbPlatnosc2 = new System.Windows.Forms.Label();
            this.LbNawynos2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LbKwota = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CbLista1
            // 
            this.CbLista1.BackColor = System.Drawing.Color.White;
            this.CbLista1.DropDownHeight = 68;
            this.CbLista1.ForeColor = System.Drawing.Color.Goldenrod;
            this.CbLista1.FormattingEnabled = true;
            this.CbLista1.IntegralHeight = false;
            this.CbLista1.Location = new System.Drawing.Point(74, 134);
            this.CbLista1.Name = "CbLista1";
            this.CbLista1.Size = new System.Drawing.Size(150, 21);
            this.CbLista1.TabIndex = 0;
            // 
            // CbLista2
            // 
            this.CbLista2.BackColor = System.Drawing.Color.White;
            this.CbLista2.DropDownHeight = 68;
            this.CbLista2.ForeColor = System.Drawing.Color.Goldenrod;
            this.CbLista2.FormattingEnabled = true;
            this.CbLista2.IntegralHeight = false;
            this.CbLista2.Location = new System.Drawing.Point(74, 172);
            this.CbLista2.Name = "CbLista2";
            this.CbLista2.Size = new System.Drawing.Size(150, 21);
            this.CbLista2.TabIndex = 1;
            // 
            // CbLista3
            // 
            this.CbLista3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CbLista3.DropDownHeight = 68;
            this.CbLista3.ForeColor = System.Drawing.Color.Goldenrod;
            this.CbLista3.FormattingEnabled = true;
            this.CbLista3.IntegralHeight = false;
            this.CbLista3.Location = new System.Drawing.Point(74, 209);
            this.CbLista3.Name = "CbLista3";
            this.CbLista3.Size = new System.Drawing.Size(150, 21);
            this.CbLista3.TabIndex = 2;
            // 
            // CbLista4
            // 
            this.CbLista4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CbLista4.DropDownHeight = 68;
            this.CbLista4.ForeColor = System.Drawing.Color.Goldenrod;
            this.CbLista4.FormattingEnabled = true;
            this.CbLista4.IntegralHeight = false;
            this.CbLista4.Location = new System.Drawing.Point(74, 247);
            this.CbLista4.Name = "CbLista4";
            this.CbLista4.Size = new System.Drawing.Size(150, 21);
            this.CbLista4.TabIndex = 3;
            // 
            // LbZamowienie
            // 
            this.LbZamowienie.AutoSize = true;
            this.LbZamowienie.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbZamowienie.Location = new System.Drawing.Point(349, 22);
            this.LbZamowienie.Name = "LbZamowienie";
            this.LbZamowienie.Size = new System.Drawing.Size(229, 42);
            this.LbZamowienie.TabIndex = 12;
            this.LbZamowienie.Text = "Zamówienie";
            // 
            // TbIlosc1
            // 
            this.TbIlosc1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TbIlosc1.Location = new System.Drawing.Point(281, 136);
            this.TbIlosc1.Name = "TbIlosc1";
            this.TbIlosc1.Size = new System.Drawing.Size(100, 13);
            this.TbIlosc1.TabIndex = 13;
            this.TbIlosc1.Text = "0";
            // 
            // TbIlosc2
            // 
            this.TbIlosc2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TbIlosc2.Location = new System.Drawing.Point(281, 173);
            this.TbIlosc2.Name = "TbIlosc2";
            this.TbIlosc2.Size = new System.Drawing.Size(100, 13);
            this.TbIlosc2.TabIndex = 14;
            this.TbIlosc2.Text = "0";
            // 
            // TbIlosc3
            // 
            this.TbIlosc3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TbIlosc3.Location = new System.Drawing.Point(281, 210);
            this.TbIlosc3.Name = "TbIlosc3";
            this.TbIlosc3.Size = new System.Drawing.Size(100, 13);
            this.TbIlosc3.TabIndex = 15;
            this.TbIlosc3.Text = "0";
            // 
            // TbIlosc4
            // 
            this.TbIlosc4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TbIlosc4.Location = new System.Drawing.Point(281, 248);
            this.TbIlosc4.Name = "TbIlosc4";
            this.TbIlosc4.Size = new System.Drawing.Size(100, 13);
            this.TbIlosc4.TabIndex = 16;
            this.TbIlosc4.Text = "0";
            // 
            // LbProdukt
            // 
            this.LbProdukt.AutoSize = true;
            this.LbProdukt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbProdukt.Location = new System.Drawing.Point(70, 98);
            this.LbProdukt.Name = "LbProdukt";
            this.LbProdukt.Size = new System.Drawing.Size(91, 24);
            this.LbProdukt.TabIndex = 17;
            this.LbProdukt.Text = "Produkty";
            // 
            // LbIlosc
            // 
            this.LbIlosc.AutoSize = true;
            this.LbIlosc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbIlosc.Location = new System.Drawing.Point(277, 98);
            this.LbIlosc.Name = "LbIlosc";
            this.LbIlosc.Size = new System.Drawing.Size(53, 24);
            this.LbIlosc.TabIndex = 18;
            this.LbIlosc.Text = "Ilość";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(205, 362);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 24);
            this.label1.TabIndex = 19;
            this.label1.Text = "Cena";
            // 
            // TbCena
            // 
            this.TbCena.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TbCena.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TbCena.Location = new System.Drawing.Point(284, 365);
            this.TbCena.Name = "TbCena";
            this.TbCena.Size = new System.Drawing.Size(100, 19);
            this.TbCena.TabIndex = 20;
            this.TbCena.Text = "0";
            // 
            // BtPrzelicz
            // 
            this.BtPrzelicz.FlatAppearance.BorderSize = 0;
            this.BtPrzelicz.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtPrzelicz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtPrzelicz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtPrzelicz.Location = new System.Drawing.Point(178, 389);
            this.BtPrzelicz.Name = "BtPrzelicz";
            this.BtPrzelicz.Size = new System.Drawing.Size(97, 29);
            this.BtPrzelicz.TabIndex = 21;
            this.BtPrzelicz.Text = "Przelicz";
            this.BtPrzelicz.UseVisualStyleBackColor = true;
            this.BtPrzelicz.Click += new System.EventHandler(this.BtPrzelicz_Click);
            // 
            // LbPlatnosc
            // 
            this.LbPlatnosc.AutoSize = true;
            this.LbPlatnosc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbPlatnosc.Location = new System.Drawing.Point(70, 287);
            this.LbPlatnosc.Name = "LbPlatnosc";
            this.LbPlatnosc.Size = new System.Drawing.Size(93, 24);
            this.LbPlatnosc.TabIndex = 22;
            this.LbPlatnosc.Text = "Płatność";
            // 
            // CbPlatnosc
            // 
            this.CbPlatnosc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CbPlatnosc.DropDownHeight = 68;
            this.CbPlatnosc.ForeColor = System.Drawing.Color.Goldenrod;
            this.CbPlatnosc.FormattingEnabled = true;
            this.CbPlatnosc.IntegralHeight = false;
            this.CbPlatnosc.Location = new System.Drawing.Point(74, 323);
            this.CbPlatnosc.Name = "CbPlatnosc";
            this.CbPlatnosc.Size = new System.Drawing.Size(150, 21);
            this.CbPlatnosc.TabIndex = 23;
            // 
            // CbNawynos
            // 
            this.CbNawynos.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CbNawynos.DropDownHeight = 68;
            this.CbNawynos.ForeColor = System.Drawing.Color.Goldenrod;
            this.CbNawynos.FormattingEnabled = true;
            this.CbNawynos.IntegralHeight = false;
            this.CbNawynos.Location = new System.Drawing.Point(281, 323);
            this.CbNawynos.Name = "CbNawynos";
            this.CbNawynos.Size = new System.Drawing.Size(100, 21);
            this.CbNawynos.TabIndex = 24;
            // 
            // LbNawynos
            // 
            this.LbNawynos.AutoSize = true;
            this.LbNawynos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbNawynos.Location = new System.Drawing.Point(280, 287);
            this.LbNawynos.Name = "LbNawynos";
            this.LbNawynos.Size = new System.Drawing.Size(101, 24);
            this.LbNawynos.TabIndex = 25;
            this.LbNawynos.Text = "Na wynos";
            // 
            // BtZatwierdz
            // 
            this.BtZatwierdz.FlatAppearance.BorderSize = 0;
            this.BtZatwierdz.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtZatwierdz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtZatwierdz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtZatwierdz.Location = new System.Drawing.Point(281, 389);
            this.BtZatwierdz.Name = "BtZatwierdz";
            this.BtZatwierdz.Size = new System.Drawing.Size(97, 29);
            this.BtZatwierdz.TabIndex = 26;
            this.BtZatwierdz.Text = "Zatwierdz";
            this.BtZatwierdz.UseVisualStyleBackColor = true;
            this.BtZatwierdz.Click += new System.EventHandler(this.BtZatwierdz_Click);
            // 
            // LbPodsumowanie
            // 
            this.LbPodsumowanie.AutoSize = true;
            this.LbPodsumowanie.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbPodsumowanie.Location = new System.Drawing.Point(469, 90);
            this.LbPodsumowanie.Name = "LbPodsumowanie";
            this.LbPodsumowanie.Size = new System.Drawing.Size(0, 33);
            this.LbPodsumowanie.TabIndex = 27;
            // 
            // LbProdukt2
            // 
            this.LbProdukt2.AutoSize = true;
            this.LbProdukt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbProdukt2.Location = new System.Drawing.Point(568, 136);
            this.LbProdukt2.Name = "LbProdukt2";
            this.LbProdukt2.Size = new System.Drawing.Size(0, 24);
            this.LbProdukt2.TabIndex = 28;
            // 
            // LbLista
            // 
            this.LbLista.AutoSize = true;
            this.LbLista.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbLista.Location = new System.Drawing.Point(511, 173);
            this.LbLista.Name = "LbLista";
            this.LbLista.Size = new System.Drawing.Size(0, 24);
            this.LbLista.TabIndex = 29;
            // 
            // LbPlatnosc2
            // 
            this.LbPlatnosc2.AutoSize = true;
            this.LbPlatnosc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbPlatnosc2.Location = new System.Drawing.Point(511, 326);
            this.LbPlatnosc2.Name = "LbPlatnosc2";
            this.LbPlatnosc2.Size = new System.Drawing.Size(0, 24);
            this.LbPlatnosc2.TabIndex = 30;
            // 
            // LbNawynos2
            // 
            this.LbNawynos2.AutoSize = true;
            this.LbNawynos2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbNawynos2.Location = new System.Drawing.Point(511, 362);
            this.LbNawynos2.Name = "LbNawynos2";
            this.LbNawynos2.Size = new System.Drawing.Size(0, 24);
            this.LbNawynos2.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(511, 411);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 24);
            this.label2.TabIndex = 32;
            // 
            // LbKwota
            // 
            this.LbKwota.AutoSize = true;
            this.LbKwota.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbKwota.Location = new System.Drawing.Point(511, 287);
            this.LbKwota.Name = "LbKwota";
            this.LbKwota.Size = new System.Drawing.Size(0, 24);
            this.LbKwota.TabIndex = 33;
            // 
            // Zamowienie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.LbKwota);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LbNawynos2);
            this.Controls.Add(this.LbPlatnosc2);
            this.Controls.Add(this.LbLista);
            this.Controls.Add(this.LbProdukt2);
            this.Controls.Add(this.LbPodsumowanie);
            this.Controls.Add(this.BtZatwierdz);
            this.Controls.Add(this.LbNawynos);
            this.Controls.Add(this.CbNawynos);
            this.Controls.Add(this.CbPlatnosc);
            this.Controls.Add(this.LbPlatnosc);
            this.Controls.Add(this.BtPrzelicz);
            this.Controls.Add(this.TbCena);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LbIlosc);
            this.Controls.Add(this.LbProdukt);
            this.Controls.Add(this.TbIlosc4);
            this.Controls.Add(this.TbIlosc3);
            this.Controls.Add(this.TbIlosc2);
            this.Controls.Add(this.TbIlosc1);
            this.Controls.Add(this.LbZamowienie);
            this.Controls.Add(this.CbLista4);
            this.Controls.Add(this.CbLista3);
            this.Controls.Add(this.CbLista2);
            this.Controls.Add(this.CbLista1);
            this.Name = "Zamowienie";
            this.Size = new System.Drawing.Size(938, 460);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CbLista1;
        private System.Windows.Forms.ComboBox CbLista2;
        private System.Windows.Forms.ComboBox CbLista3;
        private System.Windows.Forms.ComboBox CbLista4;
        private System.Windows.Forms.Label LbZamowienie;
        private System.Windows.Forms.TextBox TbIlosc1;
        private System.Windows.Forms.TextBox TbIlosc2;
        private System.Windows.Forms.TextBox TbIlosc3;
        private System.Windows.Forms.TextBox TbIlosc4;
        private System.Windows.Forms.Label LbProdukt;
        private System.Windows.Forms.Label LbIlosc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TbCena;
        private System.Windows.Forms.Button BtPrzelicz;
        private System.Windows.Forms.Label LbPlatnosc;
        private System.Windows.Forms.ComboBox CbPlatnosc;
        private System.Windows.Forms.ComboBox CbNawynos;
        private System.Windows.Forms.Label LbNawynos;
        private System.Windows.Forms.Button BtZatwierdz;
        private System.Windows.Forms.Label LbPodsumowanie;
        private System.Windows.Forms.Label LbProdukt2;
        private System.Windows.Forms.Label LbLista;
        private System.Windows.Forms.Label LbPlatnosc2;
        private System.Windows.Forms.Label LbNawynos2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LbKwota;
    }
}
