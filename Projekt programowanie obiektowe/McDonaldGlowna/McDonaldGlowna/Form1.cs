﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class Form1 : Form
    {
        int dlugosc;
        bool Hidden;
        public Form1()
        {
            InitializeComponent();
            dlugosc = TlpMenu.Width;
            Hidden = false;
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void menu_Click(object sender, EventArgs e) // odowlanie do menu.cs
        {
            PbMenu.BringToFront();
        }


        //rozwijalne menu
        private void ikona_Click(object sender, EventArgs e)
        {

            if (Hidden)
            {
                TlpMenu.Width = TlpMenu.Width = 232; //rozwija menu
                BtStronaglowna.Text = "Strona główna";
                BtRestauracje.Text = "Restauracje";
                BtZamowienia.Text = "Zamówienia";
                BtWartosci.Text = "Tabela wartości";
                BtMenu.Text = "Menu";

                if (TlpMenu.Width >= dlugosc)
                {
                    test.Start();
                    Hidden = false;
                    this.Refresh();

                }
            }
            else
            {
                TlpMenu.Width = TlpMenu.Width = 75; //zwija menu
                BtStronaglowna.Text = "";
                BtRestauracje.Text = "";
                BtZamowienia.Text = "";
                BtWartosci.Text = "";
                BtMenu.Text = "";



                if (TlpMenu.Width <= 120)
                {
                    test.Start();
                    Hidden = true;
                    this.Refresh();
                }

            }

        }

        private void BtStronaglowna_Click(object sender, EventArgs e)
        {
            PbStronaglowna.BringToFront();
        }

        private void BtExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtRestauracje_Click(object sender, EventArgs e)
        {
            restauracjee1.BringToFront();
        }

        private void BtZamowienia_Click(object sender, EventArgs e)
        {
            PbZamowienie.BringToFront();
        }

        private void BtWartosci_Click(object sender, EventArgs e)
        {
            tabelaWartosci1.BringToFront();
        }
    }
}



