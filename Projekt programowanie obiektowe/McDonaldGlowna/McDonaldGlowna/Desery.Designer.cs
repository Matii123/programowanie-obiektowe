﻿namespace McDonaldGlowna
{
    partial class Desery
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.obrazdeser1 = new System.Windows.Forms.PictureBox();
            this.obrazdeser3 = new System.Windows.Forms.PictureBox();
            this.obrazdeser2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.obrazdeser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazdeser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazdeser2)).BeginInit();
            this.SuspendLayout();
            // 
            // obrazdeser1
            // 
            this.obrazdeser1.BackColor = System.Drawing.Color.White;
            this.obrazdeser1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.obrazdeser1.Image = global::McDonaldGlowna.Properties.Resources.Ciastko_apple_pie;
            this.obrazdeser1.Location = new System.Drawing.Point(3, 71);
            this.obrazdeser1.Name = "obrazdeser1";
            this.obrazdeser1.Size = new System.Drawing.Size(242, 208);
            this.obrazdeser1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazdeser1.TabIndex = 1;
            this.obrazdeser1.TabStop = false;
            this.obrazdeser1.MouseEnter += new System.EventHandler(this.obrazdeser1_MouseEnter);
            this.obrazdeser1.MouseLeave += new System.EventHandler(this.obrazdeser1_MouseLeave);
            // 
            // obrazdeser3
            // 
            this.obrazdeser3.BackColor = System.Drawing.Color.White;
            this.obrazdeser3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.obrazdeser3.Image = global::McDonaldGlowna.Properties.Resources.lodowemarzenie1;
            this.obrazdeser3.Location = new System.Drawing.Point(584, 71);
            this.obrazdeser3.Name = "obrazdeser3";
            this.obrazdeser3.Size = new System.Drawing.Size(242, 208);
            this.obrazdeser3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazdeser3.TabIndex = 3;
            this.obrazdeser3.TabStop = false;
            this.obrazdeser3.Click += new System.EventHandler(this.obrazdeser3_Click);
            this.obrazdeser3.MouseEnter += new System.EventHandler(this.obrazdeser3_MouseEnter);
            this.obrazdeser3.MouseLeave += new System.EventHandler(this.obrazdeser3_MouseLeave);
            // 
            // obrazdeser2
            // 
            this.obrazdeser2.BackColor = System.Drawing.Color.White;
            this.obrazdeser2.Image = global::McDonaldGlowna.Properties.Resources.McFlurry;
            this.obrazdeser2.Location = new System.Drawing.Point(301, 71);
            this.obrazdeser2.Name = "obrazdeser2";
            this.obrazdeser2.Size = new System.Drawing.Size(242, 208);
            this.obrazdeser2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazdeser2.TabIndex = 2;
            this.obrazdeser2.TabStop = false;
            this.obrazdeser2.MouseEnter += new System.EventHandler(this.obrazdeser2_MouseEnter);
            this.obrazdeser2.MouseLeave += new System.EventHandler(this.obrazdeser2_MouseLeave);
            // 
            // Desery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.obrazdeser1);
            this.Controls.Add(this.obrazdeser3);
            this.Controls.Add(this.obrazdeser2);
            this.Name = "Desery";
            this.Size = new System.Drawing.Size(838, 356);
            ((System.ComponentModel.ISupportInitialize)(this.obrazdeser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazdeser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazdeser2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox obrazdeser1;
        private System.Windows.Forms.PictureBox obrazdeser2;
        private System.Windows.Forms.PictureBox obrazdeser3;
    }
}
