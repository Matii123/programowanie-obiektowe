﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class Zamowienie : UserControl
    {
        Napoj CocaCola = new Napoj("CocaCola", 4);
        Napoj Fanta = new Napoj("Fanta", 4);
        Napoj Sprite = new Napoj("Sprite", 4);
        Napoj Woda = new Napoj("Woda mineralna", 4);
        Burger BigMac = new Burger("BigMac", 10);
        Burger McRoyal = new Burger("McRoyal", 7);
        Burger WieśMac = new Burger("WieśMac", 10);
        Burger McChicken = new Burger("McChicken", 10);
        Burger McDouble = new Burger("McDouble", 10);
        Burger Cheeseburger = new Burger("Cheeseburger", 5);
        Burger Chikker = new Burger("Chikker", 6);
        Burger Kurczakburger = new Burger("Kurczakburger", 6);
        Burger JalapeñoBurger = new Burger("Jalapeño Burger", 4);
        Deser McFlurry = new Deser("McFlurry", 5);
        Deser Lody = new Deser("Lody z polewa", 4);
        Deser CiastkoSezonowe = new Deser("Ciastko Sezonowe", 9);
        Deser Owocojogurt = new Deser("Owocojogurt", 3);
        Deser LodoweMarzenie = new Deser("Lodowe Marzenie", 7);
        Deser Shake = new Deser("Shake", 8);

        Koszyk koszyk = new Koszyk();

        List<Produkt> lista_produktow = new List<Produkt>();

        bool przeliczenie = false;

        public Zamowienie()
        {
            InitializeComponent();

            lista_produktow.Add(CocaCola);
            lista_produktow.Add(Fanta);
            lista_produktow.Add(Sprite);
            lista_produktow.Add(BigMac);
            lista_produktow.Add(McRoyal);
            lista_produktow.Add(McFlurry);
            lista_produktow.Add(WieśMac);
            lista_produktow.Add(Lody);
            lista_produktow.Add(CiastkoSezonowe);
            lista_produktow.Add(Owocojogurt);
            lista_produktow.Add(LodoweMarzenie);
            lista_produktow.Add(Shake);
            lista_produktow.Add(McChicken);
            lista_produktow.Add(McDouble);
            lista_produktow.Add(Cheeseburger);
            lista_produktow.Add(Woda);
            lista_produktow.Add(Chikker);
            lista_produktow.Add(Kurczakburger);
            lista_produktow.Add(JalapeñoBurger);


            //dodawanie produktow do listy rozwijanej
            foreach (var i in lista_produktow)
            {
                CbLista1.Items.Add(i.nazwa);
                CbLista2.Items.Add(i.nazwa);
                CbLista3.Items.Add(i.nazwa);
                CbLista4.Items.Add(i.nazwa);
            }

            CbPlatnosc.Items.Add("Karta");
            CbPlatnosc.Items.Add("Gotówka");

            CbNawynos.Items.Add("Tak");
            CbNawynos.Items.Add("Nie");

        }

        private void BtPrzelicz_Click(object sender, EventArgs e) 
        {
            przeliczenie = true;
            koszyk.ilosc_produktow.Clear();
            koszyk.produkty_koszyk.Clear();

            var a = CbLista1.SelectedItem;
            var b = CbLista2.SelectedItem;
            var c = CbLista3.SelectedItem;
            var d = CbLista4.SelectedItem;

            //ostrzezenie przed nie wybraniem produktu
            if (a == null && b == null && c == null && d == null)
            {
                MessageBox.Show("Wybierz produkt!");
            }
            //dodadnie wybranego produktu z listy do koszyka
            foreach (var i in lista_produktow)
            {
                if (a == null)
                {
                }
                else if (a.ToString() == i.nazwa)
                {
                    koszyk.Dodajdokoszyka(i, int.Parse(TbIlosc1.Text));
                }
                else
                {
                }
            }
            foreach (var i in lista_produktow)
            {
                if (b == null)
                {
                }
                else if (b.ToString() == i.nazwa)
                {
                    koszyk.Dodajdokoszyka(i, int.Parse(TbIlosc2.Text));
                }
                else
                {
                }
            }
            foreach (var i in lista_produktow)
            {
                if (c == null)
                {
                }
                else if (c.ToString() == i.nazwa)
                {
                    koszyk.Dodajdokoszyka(i, int.Parse(TbIlosc3.Text));
                }
                else
                {
                }
            }
            foreach (var i in lista_produktow)
            {
                if (d == null)
                {
                }
                else if (d.ToString() == i.nazwa)
                {
                    koszyk.Dodajdokoszyka(i, int.Parse(TbIlosc4.Text));
                }
                else
                {
                }
            }
            //laczna kwota do zaplaty
            TbCena.Text = koszyk.Koszt().ToString()+"zł";
        }

        private void BtZatwierdz_Click(object sender, EventArgs e)
        {
            //sprawdzenie czy zostaly wypelnione wszystkie wymagane pole
            if ((CbLista1.SelectedItem != null || CbLista2.SelectedItem != null || CbLista3.SelectedItem != null || CbLista4.SelectedItem != null) && CbNawynos.SelectedItem != null && CbPlatnosc.SelectedItem != null && przeliczenie == true)
            {
                koszyk.DodajNumerek();
                LbPodsumowanie.Text = "Podsumowanie zamówienia";
                LbProdukt2.Text = "Zamówione produkty";
                LbLista.Text = koszyk.WyswietlProdukty();
                LbKwota.Text = "Łączna kwota: " + koszyk.Koszt().ToString() +"zł";
                LbPlatnosc2.Text = "Płatność: " + CbPlatnosc.SelectedItem.ToString();
                LbNawynos2.Text = "Na wynos: " + CbNawynos.SelectedItem.ToString();
                label2.Text = "Numerek zamówienia to: " + koszyk.numerek_zamowienia.ToString();
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola lub przelicz wartość");
            }
        }
    }
}
