﻿namespace McDonaldGlowna
{
    partial class Napoje
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.obraznapoj3 = new System.Windows.Forms.PictureBox();
            this.obraznapoj2 = new System.Windows.Forms.PictureBox();
            this.obraznapoj1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.obraznapoj3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obraznapoj2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obraznapoj1)).BeginInit();
            this.SuspendLayout();
            // 
            // obraznapoj3
            // 
            this.obraznapoj3.Image = global::McDonaldGlowna.Properties.Resources.Kawa1;
            this.obraznapoj3.Location = new System.Drawing.Point(593, 60);
            this.obraznapoj3.Name = "obraznapoj3";
            this.obraznapoj3.Size = new System.Drawing.Size(242, 208);
            this.obraznapoj3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obraznapoj3.TabIndex = 3;
            this.obraznapoj3.TabStop = false;
            this.obraznapoj3.MouseEnter += new System.EventHandler(this.obraznapoj3_MouseEnter);
            this.obraznapoj3.MouseLeave += new System.EventHandler(this.obraznapoj3_MouseLeave);
            // 
            // obraznapoj2
            // 
            this.obraznapoj2.Image = global::McDonaldGlowna.Properties.Resources.Sok;
            this.obraznapoj2.Location = new System.Drawing.Point(299, 60);
            this.obraznapoj2.Name = "obraznapoj2";
            this.obraznapoj2.Size = new System.Drawing.Size(242, 208);
            this.obraznapoj2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obraznapoj2.TabIndex = 2;
            this.obraznapoj2.TabStop = false;
            this.obraznapoj2.MouseEnter += new System.EventHandler(this.obraznapoj2_MouseEnter);
            this.obraznapoj2.MouseLeave += new System.EventHandler(this.obraznapoj2_MouseLeave);
            // 
            // obraznapoj1
            // 
            this.obraznapoj1.Image = global::McDonaldGlowna.Properties.Resources.cola;
            this.obraznapoj1.Location = new System.Drawing.Point(3, 60);
            this.obraznapoj1.Name = "obraznapoj1";
            this.obraznapoj1.Size = new System.Drawing.Size(242, 208);
            this.obraznapoj1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obraznapoj1.TabIndex = 1;
            this.obraznapoj1.TabStop = false;
            this.obraznapoj1.MouseEnter += new System.EventHandler(this.obraznapoj1_MouseEnter);
            this.obraznapoj1.MouseLeave += new System.EventHandler(this.obraznapoj1_MouseLeave);
            // 
            // Napoje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.obraznapoj3);
            this.Controls.Add(this.obraznapoj2);
            this.Controls.Add(this.obraznapoj1);
            this.Name = "Napoje";
            this.Size = new System.Drawing.Size(838, 356);
            ((System.ComponentModel.ISupportInitialize)(this.obraznapoj3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obraznapoj2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obraznapoj1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox obraznapoj1;
        private System.Windows.Forms.PictureBox obraznapoj2;
        private System.Windows.Forms.PictureBox obraznapoj3;
    }
}
