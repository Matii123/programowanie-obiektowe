﻿namespace McDonaldGlowna
{
    partial class TabelaWartosci
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.WarLista1 = new System.Windows.Forms.ComboBox();
            this.Oblicz = new System.Windows.Forms.Button();
            this.TextWar1 = new System.Windows.Forms.Label();
            this.TextWar2 = new System.Windows.Forms.Label();
            this.TextWar3 = new System.Windows.Forms.Label();
            this.TextWar4 = new System.Windows.Forms.Label();
            this.TextWar5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(173, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(476, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tabela Wartości Odżywczych";
            // 
            // WarLista1
            // 
            this.WarLista1.DropDownHeight = 68;
            this.WarLista1.FormattingEnabled = true;
            this.WarLista1.IntegralHeight = false;
            this.WarLista1.Location = new System.Drawing.Point(175, 65);
            this.WarLista1.Name = "WarLista1";
            this.WarLista1.Size = new System.Drawing.Size(150, 21);
            this.WarLista1.TabIndex = 1;
            // 
            // Oblicz
            // 
            this.Oblicz.FlatAppearance.BorderSize = 0;
            this.Oblicz.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.Oblicz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Oblicz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Oblicz.Location = new System.Drawing.Point(331, 58);
            this.Oblicz.Name = "Oblicz";
            this.Oblicz.Size = new System.Drawing.Size(256, 31);
            this.Oblicz.TabIndex = 8;
            this.Oblicz.Text = "Pokaż informację o produkcie";
            this.Oblicz.UseVisualStyleBackColor = true;
            this.Oblicz.Click += new System.EventHandler(this.Oblicz_Click);
            // 
            // TextWar1
            // 
            this.TextWar1.AutoSize = true;
            this.TextWar1.BackColor = System.Drawing.Color.White;
            this.TextWar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextWar1.ForeColor = System.Drawing.Color.Black;
            this.TextWar1.Location = new System.Drawing.Point(174, 121);
            this.TextWar1.Name = "TextWar1";
            this.TextWar1.Size = new System.Drawing.Size(0, 31);
            this.TextWar1.TabIndex = 14;
            // 
            // TextWar2
            // 
            this.TextWar2.AutoSize = true;
            this.TextWar2.BackColor = System.Drawing.Color.White;
            this.TextWar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextWar2.ForeColor = System.Drawing.Color.Black;
            this.TextWar2.Location = new System.Drawing.Point(174, 152);
            this.TextWar2.Name = "TextWar2";
            this.TextWar2.Size = new System.Drawing.Size(0, 31);
            this.TextWar2.TabIndex = 15;
            // 
            // TextWar3
            // 
            this.TextWar3.AutoSize = true;
            this.TextWar3.BackColor = System.Drawing.Color.White;
            this.TextWar3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextWar3.ForeColor = System.Drawing.Color.Black;
            this.TextWar3.Location = new System.Drawing.Point(174, 183);
            this.TextWar3.Name = "TextWar3";
            this.TextWar3.Size = new System.Drawing.Size(0, 31);
            this.TextWar3.TabIndex = 16;
            // 
            // TextWar4
            // 
            this.TextWar4.AutoSize = true;
            this.TextWar4.BackColor = System.Drawing.Color.White;
            this.TextWar4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextWar4.ForeColor = System.Drawing.Color.Black;
            this.TextWar4.Location = new System.Drawing.Point(174, 214);
            this.TextWar4.Name = "TextWar4";
            this.TextWar4.Size = new System.Drawing.Size(0, 31);
            this.TextWar4.TabIndex = 17;
            // 
            // TextWar5
            // 
            this.TextWar5.AutoSize = true;
            this.TextWar5.BackColor = System.Drawing.Color.White;
            this.TextWar5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextWar5.ForeColor = System.Drawing.Color.Black;
            this.TextWar5.Location = new System.Drawing.Point(174, 245);
            this.TextWar5.Name = "TextWar5";
            this.TextWar5.Size = new System.Drawing.Size(0, 31);
            this.TextWar5.TabIndex = 18;
            // 
            // TabelaWartosci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.TextWar5);
            this.Controls.Add(this.TextWar4);
            this.Controls.Add(this.Oblicz);
            this.Controls.Add(this.WarLista1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextWar1);
            this.Controls.Add(this.TextWar2);
            this.Controls.Add(this.TextWar3);
            this.Name = "TabelaWartosci";
            this.Size = new System.Drawing.Size(838, 356);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox WarLista1;
        private System.Windows.Forms.Button Oblicz;
        private System.Windows.Forms.Label TextWar1;
        private System.Windows.Forms.Label TextWar2;
        private System.Windows.Forms.Label TextWar3;
        private System.Windows.Forms.Label TextWar4;
        private System.Windows.Forms.Label TextWar5;
    }
}
