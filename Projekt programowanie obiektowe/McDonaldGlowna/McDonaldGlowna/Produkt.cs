﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonaldGlowna
{
    abstract class Produkt: IProdukt
    {
        public string nazwa;
        public int cena;

        public Produkt(string nazwa, int cena)
        {
            this.nazwa = nazwa;
            this.cena = cena;
        }
        public string WypiszInfo()
        {
            return $"Produkt: {this.nazwa} - cena: {this.cena}";
        }
    }
}
