﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class Napoje : UserControl
    {
        public Napoje()
        {
            InitializeComponent();
        }

        private void obraznapoj1_MouseEnter(object sender, EventArgs e)
        {
            obraznapoj1.Size = new Size(262, 228);
        }

        private void obraznapoj1_MouseLeave(object sender, EventArgs e)
        {
            obraznapoj1.Size = new Size(242, 208);
        }

        private void obraznapoj2_MouseEnter(object sender, EventArgs e)
        {
            obraznapoj2.Size = new Size(262, 228);
        }

        private void obraznapoj2_MouseLeave(object sender, EventArgs e)
        {
            obraznapoj2.Size = new Size(242, 208);
        }

        private void obraznapoj3_MouseEnter(object sender, EventArgs e)
        {
            obraznapoj3.Size = new Size(262, 228);
        }

        private void obraznapoj3_MouseLeave(object sender, EventArgs e)
        {
            obraznapoj3.Size = new Size(242, 208);
        }
    }
}
