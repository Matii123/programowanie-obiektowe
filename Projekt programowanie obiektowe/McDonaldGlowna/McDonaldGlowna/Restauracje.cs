﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace McDonaldGlowna
{
    public partial class Restauracje : UserControl
    {
        public Restauracje()
        {
            InitializeComponent();
        }

        private void wyszukaj_Click(object sender, EventArgs e)
        {
            string miasto = Listamiast.Text;
            string mcdonald = napis.Text;

            try
            {
                StringBuilder queryaddress = new StringBuilder();
                queryaddress.Append("https://www.google.com/maps/search/");

                if (miasto != string.Empty)
                {
                    queryaddress.Append(miasto + "," + "+");
                }
                if (mcdonald != string.Empty)
                {
                    queryaddress.Append(mcdonald + "," + "+");
                }
                mapa.Navigate(queryaddress.ToString());
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message.ToString(), "Błąd");
            }
        }
    }
}
