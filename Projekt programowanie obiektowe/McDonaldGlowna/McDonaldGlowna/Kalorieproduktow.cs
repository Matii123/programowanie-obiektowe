﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonaldGlowna
{
    public class Kalorieproduktow: IKalorieproduktow
    {
        public string nazwa;
        public int gramy;
        public int weglowodany;
        public int tluszcze;
        public int bialko;
        public int kalorie;
        
        public Kalorieproduktow(string nazwa, int gramy, int weglowodany, int tluszcze, int bialko, int kalorie)
        {
            this.nazwa = nazwa;
            this.gramy = gramy;
            this.weglowodany = weglowodany;
            this.tluszcze = tluszcze;
            this.bialko = bialko;
            this.kalorie = kalorie;
        }
        public string Informacje()
        {
            return $"Produkt: {this.nazwa} ";
        }
        public string Informacje1()
        {
            return $"Kalorie:{this.kalorie}";
        }
        public string Informacje2()
        {
            return $"Węglowodany:{this.weglowodany}";
        }
        public string Informacje3()
        {
            return $"Tłuszcze:{this.tluszcze}";
        }
        public string Informacje4()
        {
            return $"Białko:{this.bialko}";
        }
    }
}
