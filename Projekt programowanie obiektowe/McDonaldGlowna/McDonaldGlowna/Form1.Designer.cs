﻿namespace McDonaldGlowna
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.test = new System.Windows.Forms.Timer(this.components);
            this.BtExit = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.BtZamowienia = new System.Windows.Forms.Button();
            this.BtRestauracje = new System.Windows.Forms.Button();
            this.BtMenu = new System.Windows.Forms.Button();
            this.ikona = new System.Windows.Forms.Button();
            this.BtStronaglowna = new System.Windows.Forms.Button();
            this.TlpMenu = new System.Windows.Forms.TableLayoutPanel();
            this.BtWartosci = new System.Windows.Forms.Button();
            this.LbMcDonald = new System.Windows.Forms.Label();
            this.PbStronaglowna = new McDonaldGlowna.Stronaglowna();
            this.PbMenu = new McDonaldGlowna.Menu();
            this.PbZamowienie = new McDonaldGlowna.Zamowienie();
            this.restauracjee1 = new McDonaldGlowna.Restauracje();
            this.tabelaWartosci1 = new McDonaldGlowna.TabelaWartosci();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.TlpMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtExit
            // 
            this.BtExit.FlatAppearance.BorderSize = 0;
            this.BtExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtExit.Image = global::McDonaldGlowna.Properties.Resources.button_cancel;
            this.BtExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtExit.Location = new System.Drawing.Point(1140, 1);
            this.BtExit.Name = "BtExit";
            this.BtExit.Size = new System.Drawing.Size(75, 43);
            this.BtExit.TabIndex = 5;
            this.BtExit.UseVisualStyleBackColor = true;
            this.BtExit.Click += new System.EventHandler(this.BtExit_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Image = global::McDonaldGlowna.Properties.Resources.photo;
            this.pictureBox4.Location = new System.Drawing.Point(12, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(54, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // BtZamowienia
            // 
            this.BtZamowienia.BackColor = System.Drawing.Color.Transparent;
            this.BtZamowienia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtZamowienia.FlatAppearance.BorderSize = 0;
            this.BtZamowienia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtZamowienia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtZamowienia.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtZamowienia.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BtZamowienia.Image = global::McDonaldGlowna.Properties.Resources.buy;
            this.BtZamowienia.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtZamowienia.Location = new System.Drawing.Point(3, 311);
            this.BtZamowienia.Name = "BtZamowienia";
            this.BtZamowienia.Size = new System.Drawing.Size(226, 71);
            this.BtZamowienia.TabIndex = 9;
            this.BtZamowienia.Text = "Zamówienia";
            this.BtZamowienia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtZamowienia.UseVisualStyleBackColor = false;
            this.BtZamowienia.Click += new System.EventHandler(this.BtZamowienia_Click);
            // 
            // BtRestauracje
            // 
            this.BtRestauracje.BackColor = System.Drawing.Color.Transparent;
            this.BtRestauracje.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtRestauracje.FlatAppearance.BorderSize = 0;
            this.BtRestauracje.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtRestauracje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtRestauracje.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtRestauracje.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BtRestauracje.Image = global::McDonaldGlowna.Properties.Resources.location__1_;
            this.BtRestauracje.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtRestauracje.Location = new System.Drawing.Point(3, 234);
            this.BtRestauracje.Name = "BtRestauracje";
            this.BtRestauracje.Size = new System.Drawing.Size(226, 71);
            this.BtRestauracje.TabIndex = 8;
            this.BtRestauracje.Text = "Restauracje";
            this.BtRestauracje.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtRestauracje.UseVisualStyleBackColor = false;
            this.BtRestauracje.Click += new System.EventHandler(this.BtRestauracje_Click);
            // 
            // BtMenu
            // 
            this.BtMenu.BackColor = System.Drawing.Color.Transparent;
            this.BtMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtMenu.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtMenu.FlatAppearance.BorderSize = 0;
            this.BtMenu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtMenu.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtMenu.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BtMenu.Image = global::McDonaldGlowna.Properties.Resources.hamburger__3_;
            this.BtMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtMenu.Location = new System.Drawing.Point(3, 157);
            this.BtMenu.Name = "BtMenu";
            this.BtMenu.Size = new System.Drawing.Size(226, 71);
            this.BtMenu.TabIndex = 6;
            this.BtMenu.Text = "Menu";
            this.BtMenu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtMenu.UseVisualStyleBackColor = false;
            this.BtMenu.Click += new System.EventHandler(this.menu_Click);
            // 
            // ikona
            // 
            this.ikona.FlatAppearance.BorderSize = 0;
            this.ikona.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.ikona.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ikona.Image = global::McDonaldGlowna.Properties.Resources.menu_21;
            this.ikona.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ikona.Location = new System.Drawing.Point(3, 3);
            this.ikona.Name = "ikona";
            this.ikona.Size = new System.Drawing.Size(226, 71);
            this.ikona.TabIndex = 10;
            this.ikona.UseVisualStyleBackColor = true;
            this.ikona.Click += new System.EventHandler(this.ikona_Click);
            // 
            // BtStronaglowna
            // 
            this.BtStronaglowna.BackColor = System.Drawing.Color.Transparent;
            this.BtStronaglowna.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtStronaglowna.FlatAppearance.BorderSize = 0;
            this.BtStronaglowna.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtStronaglowna.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtStronaglowna.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtStronaglowna.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BtStronaglowna.Image = global::McDonaldGlowna.Properties.Resources.house_2;
            this.BtStronaglowna.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtStronaglowna.Location = new System.Drawing.Point(3, 80);
            this.BtStronaglowna.Name = "BtStronaglowna";
            this.BtStronaglowna.Size = new System.Drawing.Size(226, 71);
            this.BtStronaglowna.TabIndex = 7;
            this.BtStronaglowna.Text = "Strona główna";
            this.BtStronaglowna.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtStronaglowna.UseVisualStyleBackColor = false;
            this.BtStronaglowna.Click += new System.EventHandler(this.BtStronaglowna_Click);
            // 
            // TlpMenu
            // 
            this.TlpMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TlpMenu.BackColor = System.Drawing.Color.Maroon;
            this.TlpMenu.ColumnCount = 1;
            this.TlpMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 232F));
            this.TlpMenu.Controls.Add(this.BtMenu, 0, 2);
            this.TlpMenu.Controls.Add(this.BtWartosci, 0, 5);
            this.TlpMenu.Controls.Add(this.BtRestauracje, 0, 3);
            this.TlpMenu.Controls.Add(this.BtZamowienia, 0, 4);
            this.TlpMenu.Controls.Add(this.ikona, 0, 0);
            this.TlpMenu.Controls.Add(this.BtStronaglowna, 0, 1);
            this.TlpMenu.Location = new System.Drawing.Point(0, 85);
            this.TlpMenu.Name = "TlpMenu";
            this.TlpMenu.RowCount = 6;
            this.TlpMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TlpMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TlpMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TlpMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TlpMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TlpMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TlpMenu.Size = new System.Drawing.Size(232, 466);
            this.TlpMenu.TabIndex = 10;
            // 
            // BtWartosci
            // 
            this.BtWartosci.BackColor = System.Drawing.Color.Transparent;
            this.BtWartosci.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtWartosci.FlatAppearance.BorderSize = 0;
            this.BtWartosci.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.BtWartosci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtWartosci.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BtWartosci.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BtWartosci.Image = global::McDonaldGlowna.Properties.Resources.info_black;
            this.BtWartosci.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtWartosci.Location = new System.Drawing.Point(3, 388);
            this.BtWartosci.Name = "BtWartosci";
            this.BtWartosci.Size = new System.Drawing.Size(226, 75);
            this.BtWartosci.TabIndex = 11;
            this.BtWartosci.Text = "Tabela wartości odżywczych";
            this.BtWartosci.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtWartosci.UseVisualStyleBackColor = false;
            this.BtWartosci.Click += new System.EventHandler(this.BtWartosci_Click);
            // 
            // LbMcDonald
            // 
            this.LbMcDonald.AutoSize = true;
            this.LbMcDonald.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbMcDonald.Location = new System.Drawing.Point(486, 12);
            this.LbMcDonald.Name = "LbMcDonald";
            this.LbMcDonald.Size = new System.Drawing.Size(223, 42);
            this.LbMcDonald.TabIndex = 11;
            this.LbMcDonald.Text = "McDonald\'s";
            // 
            // PbStronaglowna
            // 
            this.PbStronaglowna.Location = new System.Drawing.Point(238, 91);
            this.PbStronaglowna.Name = "PbStronaglowna";
            this.PbStronaglowna.Size = new System.Drawing.Size(938, 460);
            this.PbStronaglowna.TabIndex = 13;
            // 
            // PbMenu
            // 
            this.PbMenu.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PbMenu.Location = new System.Drawing.Point(238, 85);
            this.PbMenu.Name = "PbMenu";
            this.PbMenu.Size = new System.Drawing.Size(938, 460);
            this.PbMenu.TabIndex = 8;
            // 
            // PbZamowienie
            // 
            this.PbZamowienie.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PbZamowienie.Location = new System.Drawing.Point(238, 88);
            this.PbZamowienie.Name = "PbZamowienie";
            this.PbZamowienie.Size = new System.Drawing.Size(938, 454);
            this.PbZamowienie.TabIndex = 14;
            // 
            // restauracjee1
            // 
            this.restauracjee1.BackColor = System.Drawing.Color.White;
            this.restauracjee1.Location = new System.Drawing.Point(238, 82);
            this.restauracjee1.Name = "restauracjee1";
            this.restauracjee1.Size = new System.Drawing.Size(938, 469);
            this.restauracjee1.TabIndex = 15;
            // 
            // tabelaWartosci1
            // 
            this.tabelaWartosci1.BackColor = System.Drawing.Color.White;
            this.tabelaWartosci1.Location = new System.Drawing.Point(238, 82);
            this.tabelaWartosci1.Name = "tabelaWartosci1";
            this.tabelaWartosci1.Size = new System.Drawing.Size(938, 466);
            this.tabelaWartosci1.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1180, 551);
            this.Controls.Add(this.LbMcDonald);
            this.Controls.Add(this.TlpMenu);
            this.Controls.Add(this.BtExit);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.PbStronaglowna);
            this.Controls.Add(this.PbMenu);
            this.Controls.Add(this.PbZamowienie);
            this.Controls.Add(this.restauracjee1);
            this.Controls.Add(this.tabelaWartosci1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "McDonald";
            this.TransparencyKey = System.Drawing.SystemColors.Control;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.TlpMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button BtExit;
        private System.Windows.Forms.Timer test;
        private Menu PbMenu;
        private System.Windows.Forms.Button BtZamowienia;
        private System.Windows.Forms.Button BtRestauracje;
        private System.Windows.Forms.Button BtMenu;
        private System.Windows.Forms.Button ikona;
        private System.Windows.Forms.Button BtStronaglowna;
        private System.Windows.Forms.TableLayoutPanel TlpMenu;
        private System.Windows.Forms.Button BtWartosci;
        private System.Windows.Forms.Label LbMcDonald;
        private Stronaglowna PbStronaglowna;
        private Zamowienie PbZamowienie;
        private Restauracje restauracjee1;
        private TabelaWartosci tabelaWartosci1;
    }
}

