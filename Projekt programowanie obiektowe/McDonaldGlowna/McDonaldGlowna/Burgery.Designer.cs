﻿namespace McDonaldGlowna
{
    partial class Burgery
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.obrazburger3 = new System.Windows.Forms.PictureBox();
            this.obrazburger2 = new System.Windows.Forms.PictureBox();
            this.obrazburger1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.obrazburger3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazburger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazburger1)).BeginInit();
            this.SuspendLayout();
            // 
            // obrazburger3
            // 
            this.obrazburger3.Image = global::McDonaldGlowna.Properties.Resources._01_11_podwojny_mcroyal_pikantny1;
            this.obrazburger3.Location = new System.Drawing.Point(593, 61);
            this.obrazburger3.Name = "obrazburger3";
            this.obrazburger3.Size = new System.Drawing.Size(242, 208);
            this.obrazburger3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazburger3.TabIndex = 2;
            this.obrazburger3.TabStop = false;
            this.obrazburger3.MouseEnter += new System.EventHandler(this.obrazburger3_MouseEnter);
            this.obrazburger3.MouseLeave += new System.EventHandler(this.obrazburger3_MouseLeave);
            // 
            // obrazburger2
            // 
            this.obrazburger2.Image = global::McDonaldGlowna.Properties.Resources.McRoyal;
            this.obrazburger2.Location = new System.Drawing.Point(298, 61);
            this.obrazburger2.Name = "obrazburger2";
            this.obrazburger2.Size = new System.Drawing.Size(242, 208);
            this.obrazburger2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazburger2.TabIndex = 1;
            this.obrazburger2.TabStop = false;
            this.obrazburger2.MouseEnter += new System.EventHandler(this.obrazburger2_MouseEnter);
            this.obrazburger2.MouseLeave += new System.EventHandler(this.obrazburger2_MouseLeave);
            // 
            // obrazburger1
            // 
            this.obrazburger1.Image = global::McDonaldGlowna.Properties.Resources.BigMac;
            this.obrazburger1.Location = new System.Drawing.Point(3, 61);
            this.obrazburger1.Name = "obrazburger1";
            this.obrazburger1.Size = new System.Drawing.Size(242, 208);
            this.obrazburger1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazburger1.TabIndex = 0;
            this.obrazburger1.TabStop = false;
            this.obrazburger1.MouseEnter += new System.EventHandler(this.obrazburger1_MouseEnter);
            this.obrazburger1.MouseLeave += new System.EventHandler(this.obrazburger1_MouseLeave_1);
            // 
            // Burgery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.obrazburger3);
            this.Controls.Add(this.obrazburger2);
            this.Controls.Add(this.obrazburger1);
            this.Name = "Burgery";
            this.Size = new System.Drawing.Size(838, 356);
            ((System.ComponentModel.ISupportInitialize)(this.obrazburger3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazburger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazburger1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox obrazburger1;
        private System.Windows.Forms.PictureBox obrazburger2;
        private System.Windows.Forms.PictureBox obrazburger3;
    }
}
