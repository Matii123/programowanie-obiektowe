﻿namespace McDonaldGlowna
{
    partial class Stronaglowna
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.obrazslider1 = new System.Windows.Forms.PictureBox();
            this.obrazslider2 = new System.Windows.Forms.PictureBox();
            this.obrazslider3 = new System.Windows.Forms.PictureBox();
            this.obrazslider4 = new System.Windows.Forms.PictureBox();
            this.obrazslider5 = new System.Windows.Forms.PictureBox();
            this.czasslider = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider5)).BeginInit();
            this.SuspendLayout();
            // 
            // obrazslider1
            // 
            this.obrazslider1.Image = global::McDonaldGlowna.Properties.Resources.slider1;
            this.obrazslider1.Location = new System.Drawing.Point(0, 0);
            this.obrazslider1.Name = "obrazslider1";
            this.obrazslider1.Size = new System.Drawing.Size(938, 409);
            this.obrazslider1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazslider1.TabIndex = 0;
            this.obrazslider1.TabStop = false;
            // 
            // obrazslider2
            // 
            this.obrazslider2.Image = global::McDonaldGlowna.Properties.Resources.slider2;
            this.obrazslider2.Location = new System.Drawing.Point(0, 0);
            this.obrazslider2.Name = "obrazslider2";
            this.obrazslider2.Size = new System.Drawing.Size(938, 409);
            this.obrazslider2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazslider2.TabIndex = 1;
            this.obrazslider2.TabStop = false;
            // 
            // obrazslider3
            // 
            this.obrazslider3.Image = global::McDonaldGlowna.Properties.Resources.slider3;
            this.obrazslider3.Location = new System.Drawing.Point(0, 0);
            this.obrazslider3.Name = "obrazslider3";
            this.obrazslider3.Size = new System.Drawing.Size(0, 0);
            this.obrazslider3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazslider3.TabIndex = 2;
            this.obrazslider3.TabStop = false;
            // 
            // obrazslider4
            // 
            this.obrazslider4.Image = global::McDonaldGlowna.Properties.Resources.slider4;
            this.obrazslider4.Location = new System.Drawing.Point(0, 0);
            this.obrazslider4.Name = "obrazslider4";
            this.obrazslider4.Size = new System.Drawing.Size(938, 409);
            this.obrazslider4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazslider4.TabIndex = 3;
            this.obrazslider4.TabStop = false;
            // 
            // obrazslider5
            // 
            this.obrazslider5.Image = global::McDonaldGlowna.Properties.Resources.slider3;
            this.obrazslider5.Location = new System.Drawing.Point(0, 0);
            this.obrazslider5.Name = "obrazslider5";
            this.obrazslider5.Size = new System.Drawing.Size(938, 409);
            this.obrazslider5.TabIndex = 4;
            this.obrazslider5.TabStop = false;
            // 
            // czasslider
            // 
            this.czasslider.Interval = 2000;
            this.czasslider.Tick += new System.EventHandler(this.czasslider_Tick);
            // 
            // strglowna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.obrazslider3);
            this.Controls.Add(this.obrazslider1);
            this.Controls.Add(this.obrazslider2);
            this.Controls.Add(this.obrazslider5);
            this.Controls.Add(this.obrazslider4);
            this.Name = "strglowna";
            this.Size = new System.Drawing.Size(938, 408);
            this.Load += new System.EventHandler(this.strglowna_Load);
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazslider5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox obrazslider1;
        private System.Windows.Forms.PictureBox obrazslider2;
        private System.Windows.Forms.PictureBox obrazslider3;
        private System.Windows.Forms.PictureBox obrazslider4;
        private System.Windows.Forms.PictureBox obrazslider5;
        private System.Windows.Forms.Timer czasslider;
    }
}
