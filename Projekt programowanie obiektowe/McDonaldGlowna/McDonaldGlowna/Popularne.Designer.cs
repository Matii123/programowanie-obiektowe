﻿namespace McDonaldGlowna
{
    partial class Popularne
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.obrazekpopularne1 = new System.Windows.Forms.PictureBox();
            this.obrazekpopularne2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.obrazekpopularne1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazekpopularne2)).BeginInit();
            this.SuspendLayout();
            // 
            // obrazekpopularne1
            // 
            this.obrazekpopularne1.Image = global::McDonaldGlowna.Properties.Resources.PodwójnyMCRoyal;
            this.obrazekpopularne1.Location = new System.Drawing.Point(36, 18);
            this.obrazekpopularne1.Name = "obrazekpopularne1";
            this.obrazekpopularne1.Size = new System.Drawing.Size(387, 275);
            this.obrazekpopularne1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazekpopularne1.TabIndex = 0;
            this.obrazekpopularne1.TabStop = false;
            this.obrazekpopularne1.MouseEnter += new System.EventHandler(this.obrazekpopularne1_MouseEnter_1);
            this.obrazekpopularne1.MouseLeave += new System.EventHandler(this.obrazekpopularne1_MouseLeave_1);
            // 
            // obrazekpopularne2
            // 
            this.obrazekpopularne2.Image = global::McDonaldGlowna.Properties.Resources.Lodowe_Marzenie;
            this.obrazekpopularne2.Location = new System.Drawing.Point(438, 18);
            this.obrazekpopularne2.Name = "obrazekpopularne2";
            this.obrazekpopularne2.Size = new System.Drawing.Size(387, 275);
            this.obrazekpopularne2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.obrazekpopularne2.TabIndex = 4;
            this.obrazekpopularne2.TabStop = false;
            this.obrazekpopularne2.MouseEnter += new System.EventHandler(this.obrazekpopularne2_MouseEnter);
            this.obrazekpopularne2.MouseLeave += new System.EventHandler(this.obrazekpopularne2_MouseLeave);
            // 
            // Popularne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.obrazekpopularne1);
            this.Controls.Add(this.obrazekpopularne2);
            this.Name = "Popularne";
            this.Size = new System.Drawing.Size(838, 356);
            ((System.ComponentModel.ISupportInitialize)(this.obrazekpopularne1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obrazekpopularne2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox obrazekpopularne1;
        private System.Windows.Forms.PictureBox obrazekpopularne2;
    }
}
